package formative17;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/success")
public class QueryServlet extends HttpServlet {
	static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/formative17?verifyServerCertificate=false&useSSL=true";
    static final String USER = "root";
    static final String PASS = "root";
	
	protected void doPost (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Class.forName(JDBC_DRIVER);
			Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
			Statement stmt = conn.createStatement();
			
			String query = request.getParameter("query");
			String findAll = "select * from";
			String createTable = "create table";
			String addValue = "insert into";
		
			
			if(findAll.equalsIgnoreCase(query.toString().substring(0, 13))) {
				ResultSet rs = stmt.executeQuery(query);
				PrintWriter writer = response.getWriter();
				
				String respon = "<html>";
				respon += "<body>";
				respon += "<h2>Form</h2>";
				respon += "<form action=\"success\" method=\"post\">";
				respon += "<p>INPUT QUERY IN HERE :</p>";
				respon += "<textarea id=\"query\" name=\"query\" rows=\"7\" cols=\"150\"></textarea>";
				respon += "<br><br><br><br>";
				respon += "<input type=\"submit\"/>";
				respon += "</form>";
				respon += "<br><br><br>";
				respon += "<table>";
				respon += "<tr>";
				respon += "<th> id </th>";
				respon += "<th> Street </th>";
				respon += "<th> city </th>";
				respon += "<th> country </th>";
				respon += "<th> zipCode </th>";
				respon += "</tr>";
				
				while(rs.next()) {
					respon += "<tr>";
					respon += "<td>" + rs.getInt("id") + "<td>";
					respon += "<td>" + rs.getString("street") + "<td>";
					respon += "<td>" + rs.getString("city") + "<td>";
					respon += "<td>" + rs.getString("country") + "<td>";
					respon += "<td>" + rs.getInt("zipCode") + "<td>";
					respon += "</tr>";
				}
				respon += "</table>";
				respon += "</body>";
				respon += "</html>";
				
				writer.println(respon);
				
			} else if(createTable.equalsIgnoreCase(query.toString().substring(0, 11))) {
				stmt.executeUpdate(query);
				PrintWriter writer = response.getWriter();
				
				
				String respon = "<html>";
				respon += "<body>";
				respon += "<h2>Form</h2>";
				respon += "<form action=\"success\" method=\"post\">";
				respon += "<p>INPUT QUERY IN HERE :</p>";
				respon += "<textarea id=\"query\" name=\"query\" rows=\"7\" cols=\"150\"></textarea>";
				respon += "<br><br><br><br>";
				respon += "<input type=\"submit\"/>";
				respon += "</form>";
				respon += "<br><br><br>";
				respon += "<h3> Created table success </h3>";
				respon += "</body>";
				respon += "</html>";
				
				writer.println(respon);
				
			} else {
				stmt.executeUpdate(query);
				PrintWriter writer = response.getWriter();
				
				String respon = "<html>";
				respon += "<body>";
				respon += "<h2>Form</h2>";
				respon += "<form action=\"success\" method=\"post\">";
				respon += "<p>INPUT QUERY IN HERE :</p>";
				respon += "<textarea id=\"query\" name=\"query\" rows=\"7\" cols=\"150\"></textarea>";
				respon += "<br><br><br><br>";
				respon += "<input type=\"submit\"/>";
				respon += "</form>";
				respon += "<br><br><br>";
				respon += "<h3> Add Table Success </h3>";
				respon += "</body>";
				respon += "</html>";
				
				writer.println(respon);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
